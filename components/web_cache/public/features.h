// Copyright 2021 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COMPONENTS_WEB_CACHE_PUBLIC_FEATURES_H_
#define COMPONENTS_WEB_CACHE_PUBLIC_FEATURES_H_

#include "base/feature_list.h"

namespace web_cache {

extern const base::Feature kDisableWebCache;

}  // namespace web_cache

#endif  // COMPONENTS_WEB_CACHE_PUBLIC_FEATURES_H_
