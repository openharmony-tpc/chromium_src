// Copyright (c) 2022 Huawei Device Co., Ltd. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
#ifndef OHOS_COMPONENTS_OS_CRYPT_OHOS_CRYPTO_H_
#define OHOS_COMPONENTS_OS_CRYPT_OHOS_CRYPTO_H_

#include <stddef.h>
#include <string>
#include <map>
#include <mutex>

namespace crypto {
namespace ohos {

// Constants
const size_t IV_SIZE = 16;

// Get symmetric key
std::string get_symmetric_key_256(const std::string& key_name);

// Get random IV for encryption
std::string get_iv(size_t sz = IV_SIZE);

}  // end namespace ohos
}  // end namespace crypto

#endif  // OHOS_COMPONENTS_OS_CRYPT_OHOS_CRYPTO_H_
