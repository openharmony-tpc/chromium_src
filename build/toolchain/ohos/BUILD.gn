# Copyright (c) 2022 Huawei Device Co., Ltd. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import("//build/config/clang/clang.gni")
import("//build/config/compiler/compiler.gni")
import("//build/config/ohos/config.gni")
import("//build/config/ozone.gni")
import("//build/config/sysroot.gni")
import("//build/toolchain/gcc_toolchain.gni")

declare_args() {
  # Whether unstripped binaries, i.e. compiled with debug symbols, should be
  # considered runtime_deps rather than stripped ones.
  ohos_unstripped_runtime_outputs = true
}

# The ohos clang toolchains share most of the same parameters, so we have this
# wrapper around gcc_toolchain to avoid duplication of logic.
#
# Parameters:
#  - binary_prefix
#      Prefix of compiler executables.
template("ohos_clang_toolchain") {
  gcc_toolchain(target_name) {
    assert(defined(invoker.toolchain_args),
           "toolchain_args must be defined for ohos_clang_toolchain()")

    toolchain_args = {
      forward_variables_from(invoker.toolchain_args, "*")
      current_os = target_os
    }

    # Output linker map files for binary size analysis.
    enable_linker_map = true

    _prefix = rebase_path("$clang_base_path/bin", root_build_dir)
    cc = "$_prefix/clang"
    cxx = "$_prefix/clang++"
    ar = "$_prefix/llvm-ar"
    ld = cxx
    readelf = "$_prefix/llvm-readobj"
    nm = "$_prefix/llvm-nm"
    strip = rebase_path("$clang_base_path/bin/llvm-strip", root_build_dir)
    use_unstripped_as_runtime_outputs = ohos_unstripped_runtime_outputs

    # Don't use .cr.so for loadable_modules since they are always loaded via
    # absolute path.
    loadable_module_extension = ".so"
  }
}

ohos_clang_toolchain("ohos_clang_arm") {
  toolchain_args = {
    current_cpu = "arm"
  }
}

ohos_clang_toolchain("ohos_clang_arm64") {
  toolchain_args = {
    current_cpu = "arm64"
  }
}

ohos_clang_toolchain("ohos_clang_x64") {
  toolchain_args = {
    current_cpu = "x64"
  }
}
