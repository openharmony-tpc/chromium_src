/*
 * Copyright (c) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NWEB_H
#define NWEB_H

#include <list>
#include <map>
#include <memory>
#include <string>

#include "nweb_accessibility_event_callback.h"
#include "nweb_accessibility_node_info.h"
#include "nweb_download_callback.h"
#include "nweb_drag_data.h"
#include "nweb_export.h"
#include "nweb_find_callback.h"
#include "nweb_history_list.h"
#include "nweb_hit_testresult.h"
#include "nweb_javascript_result_callback.h"
#include "nweb_preference.h"
#include "nweb_release_surface_callback.h"
#include "nweb_value_callback.h"
#include "nweb_web_message.h"

namespace OHOS::NWeb {
class NWebHandler;
class NWebValue;

/**
 * @brief Describes how pixel bits encoder color data.
 */
enum class ImageColorType {
    // Unknown color type.
    COLOR_TYPE_UNKNOWN = -1,

    // RGBA with 8 bits per pixel (32bits total).
    COLOR_TYPE_RGBA_8888 = 0,

    // BGRA with 8 bits per pixel (32bits total).
    COLOR_TYPE_BGRA_8888 = 1,
};

/**
 * @brief Describes how to interpret the alpha value of a pixel.
 */
enum class ImageAlphaType {
    // Unknown alpha type.
    ALPHA_TYPE_UNKNOWN = -1,

    // No transparency. The alpha component is ignored.
    ALPHA_TYPE_OPAQUE = 0,

    // Transparency with pre-multiplied alpha component.
    ALPHA_TYPE_PREMULTIPLIED = 1,

    // Transparency with post-multiplied alpha component.
    ALPHA_TYPE_POSTMULTIPLIED = 2,
};
struct OHOS_NWEB_EXPORT NWebInitArgs {
    std::string dump_path = "";
    bool frame_info_dump = false;
    std::list<std::string> web_engine_args_to_add;
    std::list<std::string> web_engine_args_to_delete;
    bool multi_renderer_process = false;
    bool is_enhance_surface = false;
    bool is_popup = false;
};

struct OHOS_NWEB_EXPORT NWebCreateInfo {
    /* size info */
    uint32_t width = 0;
    uint32_t height = 0;

    /* output frame cb */
    std::function<bool(const char*, uint32_t, uint32_t)> output_render_frame =
    nullptr;

    /* init args */
    NWebInitArgs init_args;

    /* rs producer surface, for acquiring elgsurface from ohos */
    void *producer_surface = nullptr;
    void* enhance_surface_info = nullptr;
};

enum class OHOS_NWEB_EXPORT DragAction {
    DRAG_START = 0,
    DRAG_ENTER,
    DRAG_LEAVE,
    DRAG_OVER,
    DRAG_DROP,
    DRAG_END,
    DRAG_CANCEL,
};

struct OHOS_NWEB_EXPORT DragEvent {
    double x;
    double y;
    DragAction action;
};

enum class BlurReason : int32_t {
    FOCUS_SWITCH = 0,
    WINDOW_BLUR = 1,
    FRAME_DESTROY = 2,
};

enum class FocusReason : int32_t {
    FOCUS_DEFAULT = 0,
    EVENT_REQUEST = 1,
};

struct OHOS_NWEB_EXPORT NWebDOHConfig {
  int doh_mode = -1;
  std::string doh_config = "";
};

enum class NestedScrollMode : int32_t {
    SELF_ONLY = 0,
    SELF_FIRST = 1,
    PARENT_FIRST = 2,
    PARALLEL = 3,
};

using ScriptItems = std::map<std::string, std::vector<std::string>>;
using WebState = std::shared_ptr<std::vector<uint8_t>>;
using SetKeepScreenOn = std::function<void(bool)>;

class OHOS_NWEB_EXPORT NWeb : public std::enable_shared_from_this<NWeb> {
    public:
    NWeb() = default;
    virtual ~NWeb() = default;

    virtual void Resize(uint32_t width, uint32_t height, bool isKeyboard = false) = 0;

    /* lifecycle interface */
    virtual void OnPause() const = 0;
    virtual void OnContinue() const = 0;
    virtual void OnDestroy() = 0;

    /* focus event */
    virtual void OnFocus(const FocusReason& focusReason = FocusReason::FOCUS_DEFAULT) const = 0;
    virtual void OnBlur(const BlurReason& blurReason) const = 0;

    /* event interface */
    virtual void OnTouchPress(int32_t id, double x, double y, bool fromOverlay = false) = 0;
    virtual void OnTouchRelease(int32_t id, double x = 0, double y = 0, bool fromOverlay = false) = 0;
    virtual void OnTouchMove(int32_t id, double x, double y, bool fromOverlay = false) = 0;
    virtual void OnTouchCancel() = 0;
    virtual void OnNavigateBack() = 0;
    virtual bool SendKeyEvent(int32_t keyCode, int32_t keyAction) = 0;
    virtual void SendMouseWheelEvent(double x, double y, double deltaX, double deltaY) = 0;
    virtual void SendMouseEvent(int x, int y, int button, int action, int count) = 0;

    /**
     * Loads the given URL.
     *
     * @param url String: the URL of the resource to load This value cannot be
     * null.
     *
     * @return title string for the current page.
     */
    virtual int Load(const std::string& url) const = 0;
    /**
     * Gets whether this NWeb has a back history item.
     *
     * @return true if this NWeb has a back history item
     */
    virtual bool IsNavigatebackwardAllowed() const = 0;
    /**
     * Gets whether this NWeb has a forward history item.
     *
     * @return true if this NWeb has a forward history item
     */
    virtual bool IsNavigateForwardAllowed() const = 0;
    /**
     * Gets whether this NWeb has a back or forward history item for number of
     * steps.
     *
     * @param numSteps int: the negative or positive number of steps to move the
     * history
     * @return true if this NWeb has a forward history item
     */
    virtual bool CanNavigateBackOrForward(int numSteps) const = 0;
    /**
     * Goes back in the history of this NWeb.
     *
     */
    virtual void NavigateBack() const = 0;
    /**
     * Goes forward in the history of this NWeb.
     *
     */
    virtual void NavigateForward() const = 0;
    /**
     * Goes to the history item that is the number of steps away from the current item.
     *
     */
    virtual void NavigateBackOrForward(int step) const = 0;
    /**
     * Delete back and forward history list.
     */
    virtual void DeleteNavigateHistory() = 0;

    /**
     * Reloads the current URL.
     *
     */
    virtual void Reload() const = 0;
    /**
     * Performs a zoom operation in this NWeb.
     *
     * @param zoomFactor float: the zoom factor to apply. The zoom factor will be
     * clamped to the NWeb's zoom limits. This value must be in the range 0.01
     * to 100.0 inclusive.
     *
     * @return the error id.
     *
     */
    virtual int Zoom(float zoomFactor) const = 0;
    /**
     * Performs a zooming in operation in this NWeb.
     *
     * @return the error id.
     *
     */
    virtual int ZoomIn() const = 0;
    /**
     * Performs a zooming out operation in this NWeb.
     *
     * @return the error id.
     *
     */
    virtual int ZoomOut() const = 0;
    /**
     * Stops the current load.
     *
     * @param code string: javascript code
     */
    virtual void Stop() const = 0;
    /**
     * ExecuteJavaScript
     *
     */
    virtual void ExecuteJavaScript(const std::string& code) const = 0;
    /**
     * ExecuteJavaScript plus
     *
     * @param code string: javascript code
     *
     * @param callback NWebValueCallback: javascript running result
     *
     */
    virtual void ExecuteJavaScript(
            const std::string& code,
            std::shared_ptr<NWebValueCallback<std::shared_ptr<NWebMessage>>> callback,
            bool extention) const = 0;
    /**
     * Gets the NWebPreference object used to control the settings for this
     * NWeb.
     *
     * @return a NWebPreference object that can be used to control this NWeb's
     * settings This value cannot be null.
     */
    virtual const std::shared_ptr<NWebPreference> GetPreference() const = 0;
    /**
     * Gets the web id.
     *
     * @return the web id
     */
    virtual unsigned int GetWebId() const = 0;
    /**
     * Gets the last hit test result.
     *
     * @return the last HitTestResult
     */
     virtual HitTestResult GetHitTestResult() const = 0;

    /**
     * Sets the background color for this view.
     *
     * @param color int: the color of the background
     *
     */
    virtual void PutBackgroundColor(int color) const = 0;

    /**
     * Sets the initla scale for the page.
     *
     * @param scale float: the initla scale of the page.
     *
     */
    virtual void InitialScale(float scale) const = 0;
    /**
     * Sets the NWebDownloadCallback that will receive download event.
     * This will replace the current handler.
     *
     * @param downloadListener NWebDownloadCallback:
     *
     */
    virtual void PutDownloadCallback(
            std::shared_ptr<NWebDownloadCallback> downloadListener) = 0;

    /**
     * Set the NWebAccessibilityEventCallback that will receive accessibility event.
     * This will replace the current handler.
     *
     * @param accessibilityEventListener NWebDownloadCallback.
     */
    virtual void PutAccessibilityEventCallback(
        std::shared_ptr<NWebAccessibilityEventCallback> accessibilityEventListener) = 0;

     /**
     * Set the accessibility id generator that will generate accessibility id for accessibility nodes in the web.
     * This will replace the current handler.
     *
     * @param accessibilityIdGenerator Accessibility id generator.
     */
    virtual void PutAccessibilityIdGenerator(std::function<int32_t()> accessibilityIdGenerator) = 0;

    /**
     * Set the NWebHandler that will receive various notifications and
     * requests. This will replace the current handler.
     *
     * @param client NWebHandler: an implementation of NWebHandler This value
     * cannot be null.
     *
     */
    virtual void SetNWebHandler(std::shared_ptr<NWebHandler> handler) = 0;
    /**
     * Gets the NWebHandler.
     *
     * @return Gets the NWebHandler.
     */
    virtual const std::shared_ptr<NWebHandler> GetNWebHandler() const = 0;
    /**
     * Gets the title for the current page.
     *
     * @return title string for the current page.
     */
    virtual std::string Title() = 0;

    /**
     * Gets the progress for the current page.
     *
     * @return progress for the current page.
     */
    virtual int PageLoadProgress() = 0;

    /**
     * Gets the height of the HTML content.
     *
     * @return the height of the HTML content.
     */
    virtual int ContentHeight() = 0;

    /**
     * Gets the current scale of this NWeb.
     *
     * @return the current scale
     */
    virtual float Scale() = 0;

    /**
     * Loads the given URL with additional HTTP headers, specified as a map
     * from name to value. Note that if this map contains any of the headers that
     * are set by default by this NWeb, such as those controlling caching,
     * accept types or the User-Agent, their values may be overridden by this
     * NWeb's defaults.
     *
     * @param url  String: the URL of the resource to load This value cannot be
     * null.
     *
     * @param additionalHttpHeaders additionalHttpHeaders
     */
    virtual int Load(
            std::string& url,
            std::map<std::string, std::string> additionalHttpHeaders) = 0;

    /**
     * Loads the given data into this NWeb, using baseUrl as the base URL for
     * the content. The base URL is used both to resolve relative URLs and when
     * applying JavaScript's same origin policy. The historyUrl is used for the
     * history entry.
     *
     * @param baseUrl  String: the URL to use as the page's base URL. If null
     * defaults to 'about:blank'. This value may be null.
     * @param data String: the URL to use as the page's base URL. If null defaults
     * to 'about:blank'. This value may be null.
     * @param mimeType String: the MIME type of the data, e.g. 'text/html'. This
     * value may be null.
     * @param encoding String: the encoding of the data This value may be null.
     * @param historyUrl String: the URL to use as the history entry. If null
     * defaults to 'about:blank'. If non-null, this must be a valid URL. This
     * value may be null.
     */
    virtual int LoadWithDataAndBaseUrl(const std::string& baseUrl,
                                        const std::string& data,
                                        const std::string& mimeType,
                                        const std::string& encoding,
                                        const std::string& historyUrl) = 0;

    /**
     * Loads the given data into this NWeb.
     *
     * @param data String: the URL to use as the page's base URL. If null defaults
     * to 'about:blank'. This value may be null.
     * @param mimeType String: the MIME type of the data, e.g. 'text/html'. This
     * value may be null.
     * @param encoding String: the encoding of the data This value may be null.
     */
    virtual int LoadWithData(const std::string& data,
                              const std::string& mimeType,
                              const std::string& encoding) = 0;

    /**
     * RegisterArkJSfunction
     *
     * @param object_name  String: objector name
     * @param method_list vector<String>: vector list ,method list
     */
    virtual void RegisterArkJSfunction(
        const std::string& object_name,
        const std::vector<std::string>& method_list) = 0;

    /**
     * UnregisterArkJSfunction
     *
     * @param object_name  String: objector name
     * @param method_list vector<String>: vector list ,method list
     */
    virtual void UnregisterArkJSfunction(
            const std::string& object_name,
            const std::vector<std::string>& method_list) = 0;

    /**
     * SetNWebJavaScriptResultCallBack
     *
     * @param callback  NWebJavaScriptResultCallBack: callback client
     */
    virtual void SetNWebJavaScriptResultCallBack(
            std::shared_ptr<NWebJavaScriptResultCallBack> callback) = 0;

    /**
     * Set the NWebFindCallback that will receive find event.
     * This will replace the current handler.
     *
     * @param findListener NWebFindCallback : find callback
     */
    virtual void PutFindCallback(
        std::shared_ptr<NWebFindCallback> findListener) = 0;

    /**
     * Finds all instances of find on the page and highlights them,
     * asynchronously.
     *
     * @param searchStr String: target string to find.
     */
    virtual void FindAllAsync(const std::string& searchStr) const = 0;

    /**
     * Clears the highlighting surrounding text matches created by findAllAsync
     *
     */
    virtual void ClearMatches() const = 0;

    /**
     * Highlights and scrolls to the next match found by findAllAsync(String),
     * wrapping around page boundaries as necessary.
     *
     * @param forward bool: find back or forward:
     */
    virtual void FindNext(const bool forward) const = 0;

    /**
     * Saves the current view as a web archive.
     *
     * @param baseName the filename where the archive should be placed This
     * value cannot be null.
     * @param autoName if false, takes basename to be a file. If true, basename
     * is assumed to be a directory in which a filename will be chosen according
     * to the URL of the current page.
     */
    virtual void StoreWebArchive(
        const std::string& baseName,
        bool autoName,
        std::shared_ptr<NWebValueCallback<std::string>> callback) const = 0;

    /**
     * creating two ends of a message channel.
     *
     * @param ports the web message ports get from nweb.
     */
    virtual void CreateWebMessagePorts(std::vector<std::string>& ports) = 0;

    /**
     * Posts MessageEvent to the main frame.
     *
     * @param message message send to mmain frame.
     * @param ports the web message ports send to main frame.
     * @param targetUri the uri which can received the ports.
     */
    virtual void PostWebMessage(std::string& message, std::vector<std::string>& ports, std::string& targetUri) = 0;

    /**
     * close the message port.
     *
     * @param portHandle the port to close.
     */
    virtual void ClosePort(std::string& portHandle) = 0;

    /**
     * use the port to send message.
     *
     * @param portHandle the port to send message.
     * @param data the message to send.
     */
    virtual void PostPortMessage(std::string& portHandle, std::shared_ptr<NWebMessage> data) = 0;

    /**
     * set the callback of the message port.
     *
     * @param portHandle the port to set callback.
     * @param callback to reveive the result when the other port post message.
     */
    virtual void SetPortMessageCallback(std::string& portHandle,
        std::shared_ptr<NWebValueCallback<std::shared_ptr<NWebMessage>>> callback) = 0;

    virtual void SendDragEvent(const DragEvent& dragEvent) const = 0;

    /**
     * Clear ssl cache.
     */
    virtual void ClearSslCache() = 0;

    /**
     * get web page url.
     *
     * @return web page url.
     */
    virtual std::string GetUrl() const = 0;

    /**
     * Clears the client authentication certificate Cache in the Web.
     *
     */
    virtual void ClearClientAuthenticationCache() = 0;

    /**
     * set the locale name of current system setting..
     *
     * @param locale the locale name of current system setting.
     */
    virtual void UpdateLocale(const std::string& language, const std::string& region) = 0;

    /**
     * get original url of the request.
     *
     * @return original url.
     */
    virtual const std::string GetOriginalUrl() const = 0;

    /**
     * get original url of the request.
     *
     * @param data raw image data of the icon.
     * @param width width of the icon.
     * @param height height of the icon.
     * @param colorType the color type of the icon.
     * @param alphaType the alpha type of the icon.
     * @return the result of get favicon.
     */
    virtual bool GetFavicon(const void** data, size_t& width, size_t& height,
        ImageColorType& c, ImageAlphaType& alphaType) = 0;

    /**
     * set the network status, just notify the webview to change the JS navigatoer.online.
	 *
     * @param available width of the icon.
     */
    virtual void PutNetworkAvailable(bool available) = 0;

    /**
     * web has image or not.
     *
     * @param callback has image or not
     */
    virtual void HasImages(std::shared_ptr<NWebValueCallback<bool>> callback) = 0;

    /**
     * web remove cache.
     *
     * @param include_disk_files bool: if false, only the RAM cache is removed
     */
    virtual void RemoveCache(bool include_disk_files) = 0;

    /**
     * web has image or not.
     *
     * @param web has image or not
     */
    virtual std::shared_ptr<NWebHistoryList> GetHistoryList() = 0;

    /**
     * Set the NWebReleaseSurfaceCallback that will receive release surface event.
     * This will replace the current handler.
     *
     * @param releaseSurfaceListener NWebReleaseSurfaceCallback.
     */
    virtual void PutReleaseSurfaceCallback(
        std::shared_ptr<NWebReleaseSurfaceCallback> releaseSurfaceListener) = 0;

    /**
     * Get web back forward state.
     *
     * @return web back forward state.
     */
    virtual WebState SerializeWebState() = 0;

    /**
     * Restore web back forward state.
     *
     * @param web back forward state.
     */
    virtual bool RestoreWebState(WebState state) = 0;

    /**
     * Move page up.
     *
     * @param top whether move to the top.
    */
    virtual void PageUp(bool top) = 0;

    /**
     * Move page down.
     *
     * @param bottom whether move to the bottom.
    */
    virtual void PageDown(bool bottom) = 0;

    /**
     * Scroll to the position.
     *
     * @param x horizontal coordinate.
     * @param y vertical coordinate.
    */
    virtual void ScrollTo(float x, float y) = 0;

    /**
     * Scroll by the delta distance.
     *
     * @param delta_x horizontal offset.
     * @param delta_y vertical offset.
    */
    virtual void ScrollBy(float delta_x, float delta_y) = 0;

    /**
     * Slide scroll by the speed.
     *
     * @param vx horizontal slide speed.
     * @param vy vertical slide speed.
    */
    virtual void SlideScroll(float vx, float vy) = 0;

    /**
     * Get current website certificate.
     *
     * @param certChainData current website certificate array.
     * @param isSingleCert true if only get one certificate of current website,
     *                     false if get certificate chain of the website.
     * @return true if get certificate successfully, otherwise false.
    */
    virtual bool GetCertChainDerData(std::vector<std::string>& certChainData, bool isSingleCert) = 0;

    /**
     * Set screen offset.
     *
     * @param x the offset in x direction.
     * @param y the offset in y direction.
    */
    virtual void SetScreenOffSet(double x, double y) = 0;

    /**
     * Set audio muted.
     *
     * @param muted Aduio mute state.
     */
    virtual void SetAudioMuted(bool muted) = 0;

    /**
     * Set should frame submission before draw.
     *
     * @param should whether wait render frame submission.
    */
    virtual void SetShouldFrameSubmissionBeforeDraw(bool should) = 0;

    /**
     * Notify whether the popup window is initialized successfully.
     *
     * @param result whether success.
     */
    virtual void NotifyPopupWindowResult(bool result) = 0;

    /**
     * Set audio resume interval.
     *
     * @param resumeInterval Aduio resume interval.
     */
    virtual void SetAudioResumeInterval(int32_t resumeInterval) = 0;

    /**
     * Set audio exclusive state.
     *
     * @param audioExclusive Aduio exclusive state.
     */
    virtual void SetAudioExclusive(bool audioExclusive) = 0;

    /**
     * Rigest the keep srceen on interface.
     *
     * @param windowId the window id.
     * @param SetKeepScreenOn the screenon handle.
     */
    virtual void RegisterScreenLockFunction(int32_t windowId, const SetKeepScreenOn&& handle) = 0;

    /**
     * UnRigest the keep srceen on interface.
     *
     * @param windowId the window id.
     */
    virtual void UnRegisterScreenLockFunction(int32_t windowId) = 0;

    /**
     * Notify memory level.
     *
     * @param level the memory level.
     */
    virtual void NotifyMemoryLevel(int32_t level) = 0;

    /**
     * Notify webview window status.
     */
    virtual void OnWebviewHide() const = 0;
    virtual void OnWebviewShow() const = 0;

    /**
     * Get drag data.
     *
     * @return the drag data.
     */
    virtual std::shared_ptr<NWebDragData> GetOrCreateDragData() = 0;

    /**
     * Prefetch the resources required by the page, but will not execute js or
     * render the page.
     *
     * @param url  String: Which url to preresolve/preconnect.
     * @param additionalHttpHeaders Additional HTTP request header of the URL.
     */
    virtual void PrefetchPage(
        std::string& url,
        std::map<std::string, std::string> additionalHttpHeaders) = 0;

    /**
     * Set the window id.
     */
    virtual void SetWindowId(uint32_t window_id) = 0;

    /**
     * Notify that browser was occluded by other windows.
     */
    virtual void OnOccluded() const = 0;

    /**
     *Notify that browser was unoccluded by other windows.
     */
    virtual void OnUnoccluded() const = 0;

    /**
     * Set the token.
     */
    virtual void SetToken(void* token) = 0;

    /**
     * Set the nested scroll mode.
     */
    virtual void SetNestedScrollMode(const NestedScrollMode& nestedScrollMode) = 0;

    /**
     * Set enable lower the frame rate.
     */
    virtual void SetEnableLowerFrameRate(bool enabled) const = 0;

    /**
     * Set the property values for width, height, and keyboard height.
     */
    virtual void SetVirtualKeyBoardArg(int32_t width, int32_t height, double keyboard) = 0;

    /**
     * Set the virtual keyboard to override the web status.
     */
    virtual bool ShouldVirtualKeyboardOverlay() = 0;

    /**
     * Set draw rect.
     *
    */
    virtual void SetDrawRect(int32_t x, int32_t y, int32_t width, int32_t height) = 0;

    /**
     * Set draw mode.
     *
    */
    virtual void SetDrawMode(int32_t mode) = 0;

    /**
     * Create web print document adapter.
     *
    */
    virtual void* CreateWebPrintDocumentAdapter(const std::string& jobName) = 0;

    /**
     * Loads the URL with postData using "POST" method into this WebView.
     * If url is not a network URL, it will be loaded with loadUrl(String) instead.
     *
     * @param url String: the URL of the resource to load This value cannot be null.
     * @param postData the data will be passed to "POST" request,
     * whilch must be "application/x-www-form-urlencoded" encoded.
     *
     * @return title string for the current page.
     */
    virtual int PostUrl(const std::string& url, std::vector<char>& postData) = 0;

    /**
     * Inject the JavaScript before WebView load the DOM tree.
     */
    virtual void JavaScriptOnDocumentStart(const ScriptItems& scriptItems) = 0;
	
	/**
     * Execute an accessibility action on an accessibility node in the browser.
     * @param accessibilityId The id of the accessibility node.
     * @param action The action to be performed on the accessibility node.
     */
    virtual void ExecuteAction(int32_t accessibilityId, uint32_t action) const = 0;

    /**
     * Get the information of the focused accessibility node on the given accessibility node in the browser.
     * @param accessibilityId Indicate the accessibility id of the parent node of the focused accessibility node.
     * @param isAccessibilityFocus Indicate whether the focused accessibility node is accessibility focused or input
     * focused.
     * @param nodeInfo The obtained information of the accessibility node.
     * @return true if get accessibility node info successfully, otherwise false.
     */
    virtual bool GetFocusedAccessibilityNodeInfo(
        int32_t accessibilityId, bool isAccessibilityFocus, OHOS::NWeb::NWebAccessibilityNodeInfo& nodeInfo) const = 0;

    /**
     * Get the information of the accessibility node by its accessibility id in the browser.
     * @param accessibilityId The accessibility id of the accessibility node.
     * @param nodeInfo The obtained information of the accessibility node.
     * @return true if get accessibility node info successfully, otherwise false.
     */
    virtual bool GetAccessibilityNodeInfoById(
        int32_t accessibilityId, OHOS::NWeb::NWebAccessibilityNodeInfo& nodeInfo) const = 0;

    /**
     * Get the information of the accessibility node by focus move in the browser.
     * @param accessibilityId The accessibility id of the original accessibility node.
     * @param direction The focus move direction of the original accessibility node.
     * @param nodeInfo The obtained information of the accessibility node.
     * @return true if get accessibility node info successfully, otherwise false.
     */
    virtual bool GetAccessibilityNodeInfoByFocusMove(
        int32_t accessibilityId, int32_t direction, OHOS::NWeb::NWebAccessibilityNodeInfo& nodeInfo) const = 0;

    /**
     * Set the accessibility state in the browser.
     * @param state Indicate whether the accessibility state is enabled or disabled.
     */
    virtual void SetAccessibilityState(bool state) = 0;

    /**
     * RegisterArkJSfunctionExt
     *
     * @param object_name  String: objector name
     * @param method_list vector<String>: vector list ,method list
     * @param object_id int32_t: object id
     */
    virtual void RegisterArkJSfunctionExt(
        const std::string& object_name,
        const std::vector<std::string>& method_list,
        const int32_t object_id) = 0;

     /**
     * Get whether need soft keyboard.
     *
     * @return true if need soft keyboard, otherwise false.
     */
    virtual bool NeedSoftKeyboard() const = 0;

    /**
     * Discard the webview window.
     * @return true if the discarding success, otherwise false.
     */
    virtual bool Discard() = 0;

    /**
     * Reload the webview window that has been discarded before.
     * @return true if the discarded window reload success, otherwise false.
     */
    virtual bool Restore() = 0;

    /**
     * CallH5Function
     *
     * @param routing_id       int32_t: the h5 frmae routing id
     * @param h5_object_id     int32_t: the h5 side object id
     * @param h5_method_name   string:  the h5 side object method name
     * @param args             vector<shared_ptr<NWebValue>>: the call args
     */
    virtual void CallH5Function(int32_t routing_id,
                                int32_t h5_object_id,
                                const std::string h5_method_name,
                                const std::vector<std::shared_ptr<NWebValue>>& args) = 0;
};
}  // namespace OHOS::NWeb

#endif
