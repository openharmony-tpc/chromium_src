/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NWEB_JS_HTTP_AUTH_RESULT_IMPL_H
#define NWEB_JS_HTTP_AUTH_RESULT_IMPL_H

#include "nweb_js_http_auth_result.h"
#include "cef/include/cef_auth_callback.h"

namespace OHOS::NWeb {
class NWebJSHttpAuthResultImpl : public NWebJSHttpAuthResult {
 public:
  NWebJSHttpAuthResultImpl() = default;
  explicit NWebJSHttpAuthResultImpl(CefRefPtr<CefAuthCallback> callback);
  ~NWebJSHttpAuthResultImpl() = default;
  bool Confirm(std::string &userName, std::string &pwd) override;
  void Cancel() override;
  bool IsHttpAuthInfoSaved() override;
 private:
  CefRefPtr<CefAuthCallback> callback_;
};
}

#endif