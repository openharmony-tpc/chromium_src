// Copyright (c) 2023 Huawei Device Co., Ltd. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "media/mojo/services/gpu_mojo_media_client.h"

#include "base/memory/ptr_util.h"
#include "gpu/command_buffer/service/ref_counted_lock.h"
#include "gpu/config/gpu_finch_features.h"
#include "media/base/media_log.h"
#include "media/base/media_switches.h"
#include "media/gpu/ohos/codec_allocator.h"
#include "media/gpu/ohos/direct_shared_image_video_provider.h"
#include "media/gpu/ohos/ohos_video_decoder.h"
#include "media/gpu/ohos/video_frame_factory_impl.h"
#include "media/mojo/mojom/media_drm_storage.mojom.h"
#include "media/mojo/mojom/provision_fetcher.mojom.h"
#include "media/mojo/services/mojo_media_drm_storage.h"
#include "media/mojo/services/mojo_provision_fetcher.h"

namespace media {

std::unique_ptr<VideoDecoder> CreatePlatformVideoDecoder(
    const VideoDecoderTraits& traits) {
  LOG(INFO) << "CreatePlatformVideoDecoder";
  scoped_refptr<gpu::RefCountedLock> ref_counted_lock;
  ref_counted_lock = base::MakeRefCounted<gpu::RefCountedLock>();

  std::unique_ptr<SharedImageVideoProvider> image_provider =
      std::make_unique<DirectSharedImageVideoProvider>(
          traits.gpu_task_runner, traits.get_command_buffer_stub_cb,
          ref_counted_lock);

  auto frame_info_helper = FrameInfoHelper::Create(
      traits.gpu_task_runner, traits.get_command_buffer_stub_cb,
      ref_counted_lock);

  return OhosVideoDecoder::Create(
      traits.gpu_preferences, traits.gpu_feature_info,
      traits.media_log->Clone(),
      CodecAllocator::GetInstance(traits.gpu_task_runner),
      std::make_unique<VideoFrameFactoryImpl>(
          traits.gpu_task_runner, traits.gpu_preferences,
          std::move(image_provider), std::move(frame_info_helper),
          ref_counted_lock),
      ref_counted_lock);
}

absl::optional<SupportedVideoDecoderConfigs>
GetPlatformSupportedVideoDecoderConfigs(
    gpu::GpuDriverBugWorkarounds gpu_workarounds,
    gpu::GpuPreferences gpu_preferences,
    const gpu::GPUInfo& gpu_info,
    base::OnceCallback<SupportedVideoDecoderConfigs()> get_vda_configs) {
  LOG(INFO) << "GetPlatformSupportedVideoDecoderConfigs";
  return OhosVideoDecoder::GetSupportedConfigs();
}

// Not support platform audio decoder in ohos now.
std::unique_ptr<AudioDecoder> CreatePlatformAudioDecoder(
    scoped_refptr<base::SingleThreadTaskRunner> task_runner) {
  return nullptr;
}

VideoDecoderType GetPlatformDecoderImplementationType(
    gpu::GpuDriverBugWorkarounds gpu_workarounds,
    gpu::GpuPreferences gpu_preferences,
    const gpu::GPUInfo& gpu_info) {
  return VideoDecoderType::kOHOS;
}

// There is no CdmFactory on ohos now.
class CdmFactory {};
std::unique_ptr<CdmFactory> CreatePlatformCdmFactory(
    mojom::FrameInterfaceFactory* frame_interfaces) {
  return nullptr;
}

}  // namespace media